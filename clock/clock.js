const clockHandleElement = document.querySelector('#clock .clock-handle');

const clockHandleModel = {
    /**
     * @param {number} x
     */
    set rotation(rotation) {
        this.r = `${rotation}deg`;

        if (clockHandleElement) {
            clockHandleElement.style.transform = `translate(-50%, -100%) rotate(${this.r})`;
        }
    },

    /**
     * @param {array} c
     */
    set color(c) {
        this.red = c[0];
        this.green = c[1];
        this.blue = c[2];

        if (c.length === 4) {
            this.alpha = c[3];
        } else {
            this.alpha = 1;
        }

        if (clockHandleElement) {
            const red = this.red * 255;
            const green = this.green * 255;
            const blue = this.blue * 255;
            const alpha = this.alpha;
            clockHandleElement.style.borderColor = `rgba(${red}, ${green}, ${blue}, ${alpha})`;
        }
    }
};

function resetModel() {
    clockHandleModel.rotation = 0;
}

window.handle = clockHandleModel;
window.resetModel = resetModel;