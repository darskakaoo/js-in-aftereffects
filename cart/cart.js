const cartElement = document.getElementById('cart');

const cartModel = {
    /**
     * @param {number} x
     */
    set position(x) {
        this.x = `${x * 10}%`;

        if (cartElement) {
            cartElement.style.left = this.x;
        }
    }
};

function resetModel() {
    cartModel.position = 0;
}

window.cart = cartModel;
window.resetModel = resetModel;