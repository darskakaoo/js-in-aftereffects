const cartElement = document.getElementById('cart');

const cartModel = {
    /**
     * @param {array} pos
     */
    set position(pos) {
        this.x = `${pos[0] * 10}%`;
        this.y = `${pos[1]}px`;

        if (cartElement) {
            cartElement.style.left = this.x;
            cartElement.style.top = this.y;
        }
    },

    /**
     * @param {array} c
     */
    set color(c) {
        this.red = c[0];
        this.green = c[1];
        this.blue = c[2];

        if (c.length === 4) {
            this.alpha = c[3];
        } else{
            this.alpha = 1;
        }

        if (cartElement) {
            const red = this.red * 255;
            const green = this.green * 255;
            const blue = this.blue * 255;
            const alpha = this.alpha;
            cartElement.style.color = `rgba(${red}, ${green}, ${blue}, ${alpha})`;
        }
    }
};

function resetModel() {
    cartModel.position = 0;
    cartModel.color = [0, 0, 0, 1];
}

window.cart = cartModel;
window.resetModel = resetModel;