let time;
let intervalId;

const timeElement = document.getElementById('time');
const buttonsContainerElement = document.getElementById('buttons-container');

function reset() {
    time = 0;
    timeElement.innerHTML = '0s';

    if (resetModel && typeof resetModel === 'function') {
        resetModel();
    }

    const scriptText = document.getElementById('rendered-js').innerHTML;
    eval(scriptText);
}

function startAnimation() {

    if (intervalId === undefined) {
        const scriptText = document.getElementById('rendered-js').innerHTML;

        intervalId = setInterval(function () {
            time += 0.05;

            if (scriptText) {
                eval(scriptText);
            }

            timeElement.innerHTML = `${Number.parseFloat(time).toFixed(2)}s`;
        }, 50);
    }
}

function pauseAnimation() {
    if (intervalId !== undefined) {
        clearInterval(intervalId);
        intervalId = undefined;
    }
}

function stopAnimation() {
    pauseAnimation();
    reset();
}

const startButton = document.createElement('button');
startButton.innerHTML = 'Start Animation';
startButton.addEventListener('click', startAnimation);

buttonsContainerElement.appendChild(startButton);

const pauseButton = document.createElement('button');
pauseButton.innerHTML = 'Pause Animation';
pauseButton.addEventListener('click', pauseAnimation);

buttonsContainerElement.appendChild(pauseButton);

const stopButton = document.createElement('button');
stopButton.innerHTML = 'Stop Animation';
stopButton.addEventListener('click', stopAnimation);

buttonsContainerElement.appendChild(stopButton);



reset();
window.time = time;